// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import axios from 'axios';
import type { NextApiRequest, NextApiResponse } from 'next';

export default async function handler(
  _req: NextApiRequest,
  res: NextApiResponse
) {
  const params = new URLSearchParams()
  const clientId = process.env.CLIENT_ID ?? '';
  const clientSecret = process.env.CLIENT_SECRET ?? '';
  params.append('grant_type', 'client_credentials')
  params.append('scope', 'EasyCube.External.Api')
  try {
    const response = await axios.post(process.env.ACCESS_TOKEN_URL ?? '', params, {
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization:
          `Basic ${Buffer.from(`${clientId}:${clientSecret}`).toString('base64')}`,
        'Cache-Control': 'no-cache',
      }
    });
    
    //Provide exact date/time of the expiration of the token
    const now = new Date();
    now.setMinutes(now.getMinutes() + response.data.expires_in / 60);
    
    res.status(200).json({...response.data, expires_in: now.getTime()});
  } catch(error) {
    res.status(400).json( error.data );
  }
}
