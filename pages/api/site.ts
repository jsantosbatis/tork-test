// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import axios from 'axios';
import type { NextApiRequest, NextApiResponse } from 'next';
import { Site } from '../../types/Site';

type SiteResponse = {
    result: Site;
    isValid: boolean;
    errors: string[];
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  
  if(req.headers.authorization && req.method == 'GET') {
    try {
      const response = await axios.get<SiteResponse>('https://easycube-external-api-web-c2m2jq5zkw6rc.azurewebsites.net/api/Site', {
        headers:{
          'Content-Type': 'application/json',
          Authorization: req.headers.authorization,            
        }
      });
    
      res.status(200).json(response.data.result);
    } catch(error) {
      res.status(400).json(error.data);
    }
  } else {
    res.status(400);
  }
}
