import axios from "axios";
import { TOKEN_KEY } from "./constant";
import { Site } from "./types/Site";
import { Token } from "./types/Token";

const hasExpired = (moment : number) : boolean => {
  const now = Date.now();
  return moment < now;
}

const getToken = async () : Promise<Token> => {    
  const token: Token = JSON.parse(localStorage.getItem(TOKEN_KEY) ?? 'false');
  if(token && !hasExpired(token.expires_in)) {
    return token;
  } else {
    const { access_token, expires_in} = (await (axios.post<Token>('/api/token'))).data;
    localStorage.setItem(TOKEN_KEY, JSON.stringify({
      access_token,
      expires_in
    }));        
    return {
      access_token,
      expires_in
    };
  }
}

const getSite = async () : Promise<Site> => {
  const token = await getToken();
  return (await (axios.get<Site>('/api/site', {
      headers: {
          'Authorization': `Bearer ${token.access_token}`,
      }
  }))).data;
}

export { getToken, getSite };

