import { useEffect, useState } from 'react'
import { getSite } from '../service';
import { Site } from '../types/Site';


export default function Home() {
  const [currentSite, setCurrentSite] = useState<Site|null>(null);
  useEffect(() => {
    async function fetchSite() {
      const site = await getSite();
      setCurrentSite(site);
    }
    fetchSite();
  },[]);

  return (
    <div>    
      Current site name: {currentSite?.siteName}
    </div>
  )
}
